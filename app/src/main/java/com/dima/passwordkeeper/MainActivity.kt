package com.dima.passwordkeeper

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dima.passwordkeeper.viewmodel.PasswordViewModel
import com.dima.passwordkeeper.viewmodel.PasswordViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private val FIRST_LAUNCH = "first_launch"
        private val SECRET = "secret"
    }

    private var hasPermission: Boolean = false

    private var adapter = PasswordAdapter {
        return@PasswordAdapter hasPermission
    }

    private lateinit var viewModel: PasswordViewModel

    private lateinit var preferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        passwordList.adapter = adapter
        passwordList.layoutManager = LinearLayoutManager(this)

        addPasswordButton.setOnClickListener {
            addPassword()
        }

        viewModel = ViewModelProvider(this, PasswordViewModelFactory(this)).get(PasswordViewModel::class.java)
        viewModel.passwords.observe(this) { passwords ->
            adapter.setPasswords(passwords)
        }

        preferences = getSharedPreferences(packageName, 0)

        checkUser()
    }

    private fun checkUser() {
        val firstLaunch = preferences.getBoolean(FIRST_LAUNCH, true)

        if (firstLaunch) {
            viewModel.deleteAll()
            setSecret()
        } else {
            askSecret()
        }
    }

    private fun setSecret() {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_form, null)
        val edit = view.findViewById<EditText>(R.id.secretEdit)
        val dialog = AlertDialog.Builder(this)
            .setTitle(getString(R.string.set_password))
            .setView(view)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.ok)
            ) { _, _ ->
                val secret = edit.text.toString()
                val editor = preferences.edit()
                editor.putBoolean(FIRST_LAUNCH, false)
                editor.putString(SECRET, secret)
                editor.apply()

                hasPermission = true
            }
        dialog.show()
    }

    private fun askSecret() {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_form, null)
        val edit = view.findViewById<EditText>(R.id.secretEdit)
        val dialog = AlertDialog.Builder(this)
            .setTitle(getString(R.string.enter_password))
            .setView(view)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.ok)
            ) { _, _ ->
                val realSecret = preferences.getString(SECRET, null)
                if (realSecret == null) {
                    hasPermission = false
                    return@setPositiveButton
                }

                val secret = edit.text.toString()

                hasPermission = secret == realSecret
            }
        dialog.show()
    }

    private fun addPassword() {
        AddPasswordFragment().show(supportFragmentManager, null)
    }
}