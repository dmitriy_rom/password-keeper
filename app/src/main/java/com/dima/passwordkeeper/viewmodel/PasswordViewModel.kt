package com.dima.passwordkeeper.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dima.passwordkeeper.repository.Repository
import com.dima.passwordkeeper.repository.database.MyDatabase
import com.dima.passwordkeeper.repository.database.ServicePassword
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PasswordViewModel(private val context: Context): ViewModel() {

    private val repository: Repository

    var passwords: LiveData<List<ServicePassword>>

    init {
        val dao = MyDatabase.getDatabase(context).dao()
        repository = Repository(dao)

        passwords = repository.allPasswords()
    }

    fun insertPassword(password: ServicePassword) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertPassword(password)
        }
    }

    fun updatePassword(password: ServicePassword) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updatePassword(password)
        }
    }

    fun deletePassword(password: ServicePassword) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deletePassword(password)
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAll()
        }
    }
}