package com.dima.passwordkeeper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.dima.passwordkeeper.repository.database.ServicePassword

class PasswordAdapter(val askPermission: (() -> Boolean)): RecyclerView.Adapter<PasswordAdapter.PasswordViewHolder>() {

    private var passwords: List<ServicePassword> = listOf()

    fun setPasswords(passwords: List<ServicePassword>) {
        this.passwords = passwords
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PasswordViewHolder {
        return PasswordViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_password, parent, false))
    }

    override fun onBindViewHolder(holder: PasswordViewHolder, position: Int) {
        holder.bind(passwords[position])
    }

    override fun getItemCount(): Int {
        return passwords.size
    }

    inner class PasswordViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val serviceName = itemView.findViewById<TextView>(R.id.serviceName)

        private val servicePassword = itemView.findViewById<TextView>(R.id.servicePassword)

        private val toggleButton = itemView.findViewById<ImageButton>(R.id.toggleButton)

        private lateinit var password: ServicePassword
        private var visible = false

        fun bind(password: ServicePassword) {
            serviceName.text = password.serviceName
            servicePassword.text = password.value.toString()

            this.password = password

            toggleButton.setOnClickListener {
                askPermission()
            }
        }

        private fun askPermission() {
            val result = askPermission.invoke()

            if (result) {
                toggleVisibility()
            } else {
                Toast.makeText(itemView.context, itemView.context.getString(R.string.access_denied), Toast.LENGTH_SHORT).show()
            }
        }

        private fun toggleVisibility() {
            visible = !visible
            toggleButton.setImageDrawable(itemView.context.getDrawable(if (visible) R.drawable.ic_lock else R.drawable.ic_unlock))
            if (visible) {
                servicePassword.text = Crypt.decrypt(alias = password.serviceName!!, iv = password.iv!!, byteArray = password.value!!)
            } else {
                servicePassword.text = password.value.toString()
            }
        }
    }
}