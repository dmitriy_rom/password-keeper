package com.dima.passwordkeeper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.dima.passwordkeeper.repository.database.ServicePassword
import com.dima.passwordkeeper.viewmodel.PasswordViewModel
import com.dima.passwordkeeper.viewmodel.PasswordViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_add_password.*


class AddPasswordFragment : BottomSheetDialogFragment() {

    private lateinit var viewModel: PasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        saveButton.setOnClickListener {
            savePassword()
        }

        viewModel = ViewModelProvider(requireActivity(), PasswordViewModelFactory(requireContext())).get(
            PasswordViewModel::class.java
        )
    }

    private fun savePassword() {
        val name = serviceNameEdit.text.toString()
        val password = servicePasswordEdit.text.toString()

        val (iv, encryptedPassword) = encryptPassword(password, name)

        viewModel.insertPassword(ServicePassword(serviceName = name, value = encryptedPassword, iv = iv))

        dismiss()
    }

    private fun encryptPassword(password: String, alias: String): Pair<ByteArray, ByteArray> {
        return Crypt.encrypt(password, alias)
    }
}