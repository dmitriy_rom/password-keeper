package com.dima.passwordkeeper.repository

import androidx.lifecycle.LiveData
import com.dima.passwordkeeper.repository.database.Dao
import com.dima.passwordkeeper.repository.database.ServicePassword

class Repository(
    private val dao: Dao
) {
    fun allPasswords(): LiveData<List<ServicePassword>> = dao.getAllPasswords()
    suspend fun insertPassword(password: ServicePassword): Long = dao.insertPassword(password)
    suspend fun deletePassword(password: ServicePassword) = dao.deletePassword(password)
    suspend fun updatePassword(password: ServicePassword) = dao.updatePassword(password)
    suspend fun deleteAll() = dao.deleteAll()
}