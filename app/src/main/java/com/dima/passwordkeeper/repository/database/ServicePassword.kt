package com.dima.passwordkeeper.repository.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "passwords")
data class ServicePassword(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    var id: Long = 0,
    var serviceName: String?,
    var value: ByteArray?,
    var iv: ByteArray?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ServicePassword

        if (id != other.id) return false
        if (serviceName != other.serviceName) return false
        if (value != null) {
            if (other.value == null) return false
            if (!value.contentEquals(other.value)) return false
        } else if (other.value != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (serviceName?.hashCode() ?: 0)
        result = 31 * result + (value?.contentHashCode() ?: 0)
        return result
    }
}