package com.dima.passwordkeeper.repository.database

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao

@Dao
interface Dao {
    // ATTENDANCE
    @Query("SELECT * FROM passwords")
    fun getAllPasswords(): LiveData<List<ServicePassword>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPassword(password: ServicePassword): Long

    @Delete
    suspend fun deletePassword(password: ServicePassword)

    @Update
    suspend fun updatePassword(password: ServicePassword)

    @Query("DELETE FROM passwords")
    suspend fun deleteAll()
}